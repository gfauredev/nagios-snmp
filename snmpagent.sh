echo "Snmp agent : 1 install dependencies"
apt install -y snmpd

echo "Snmp agent : 2 configure snmpd"
sd "(agentaddress) .*" "\$1 161" /etc/snmp/snmpd.conf
sd "(rocommunity6? *public) .*" "\$1" /etc/snmp/snmpd.conf

systemctl restart snmpd
