#!/bin/bash

# Addresse de la machine monitorée (premier paramètre)
if [ -n "$1" ]; then
  host="$1"
else
  host="192.168.42.3"
fi

# ID de l’interface monitorée (second paramètre)
if [ -n "$2" ]; then
  ifId="$2"
else
  # ifId="1" # lo (loopback)
  ifId="2" # eth0
  # ifId="3" # eth1
fi

# Nom de la communauté de monitoring (troisième paramètre)
if [ -n "$3" ]; then
  community="$3"
else
  community="public"
fi

# Version SNMP (quatrième paramètre)
if [ -n "$4" ]; then
  snmpV="$4"
else
  snmpV="2c"
fi

# Durée de la mesure en secondes (cinquième paramètre)
if [ -n "$5" ]; then
  interval="$5"
else
  interval=10
fi

# OID SNMP du traffic sortant en octets (sixième paramètre, probablement inutile)
if [ -n "$6" ]; then
  outOctetsOID="$6"
else
  outOctetsOID=".1.3.6.1.2.1.2.2.1.16"
fi

# (Commande pour récupérer les index des interfaces réseau)
# snmpwalk -v $snmp -c $community $host IF-MIB::ifDescr
# (Commande pour récupérer le nombre d’octets sortants de l’interface monitorée)
# snmpget -v $snmp -c $community $host $outOctetsOID.$ifId | cut -d\" \" -f4

# Rassemblement de tous les paramètres pour la commande SNMP
snmpCmd="-v $snmpV -c $community $host $outOctetsOID.$ifId"

# Première lecture des octets sortants
initialOutOctets=$(snmpget $snmpCmd | cut -d" " -f4)
# Attente de interval secondes pour la mesure
sleep $interval
# Seconde lecture des octets sortants
finalOutOctets=$(snmpget $snmpCmd | cut -d" " -f4)

# Calcul de la bande passante
octets=$(($finalOutOctets - $initialOutOctets))
bandwidth=$(($octets / $interval))
# Définition des seuils de retour et d’affichage
warning=2048   # 2 Kbps
critical=51200 # 50 Kbps

# Affichage de la bande passante et retours selon les seuils
os="octets/s"
if [ $bandwidth -lt $warning ]; then
    emoji=$'😊' # Emote content
    msg=$'OK' # Message
    # emoji=$"\033[0;32m$emoji\033[0m" # Coloration verte pour terminal
    # msg=$"\033[0;32m$msg\033[0m" # Coloration verte pour terminal
    echo -e "$msg - Bande passante du trafic sortant: $bandwidth $os $emoji"
    exit 0
elif [ $bandwidth -ge $warning -a $bandwidth -lt $critical ]; then
    emoji=$'😐' # Emote pas très content
    msg=$'ALERTE' # Message
    # emoji=$"\033[0;33m$emoji\033[0m" # Coloration orange pour terminal
    # msg=$"\033[0;33m$msg\033[0m" # Coloration orange pour terminal
    echo -e "$msg - Bande passante du trafic sortant: $bandwidth $os $emoji"
    exit 1
else
    emoji=$'😠' # Emote en colère
    msg=$'CRITIQUE' # Message
    # emoji=$"\033[0;31m$emoji\033[0m" # Coloration rouge pour terminal
    # msg=$"\033[0;31m$msg\033[0m" # Coloration rouge pour terminal
    echo -e "$msg - Bande passante du trafic sortant: $bandwidth $os $emoji"
    exit 2
fi
