echo "Snmp common : 1 enable non-free repos"
sd "(deb https://deb.debian.org/debian bookworm main)" "\$1 non-free contrib" \
/etc/apt/sources.list

echo "Snmp common : 2 install dependencies"
apt update
apt install -y snmp-mibs-downloader

echo "Snmp common : 3 install mibs"
sd "(mibs :)" "# \$1" /etc/snmp/snmp.conf
download-mibs
