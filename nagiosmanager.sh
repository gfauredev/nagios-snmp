echo "Nagios manager : 1 Define user and groups"
useradd nagios
groupadd nagcmd
usermod -aG nagcmd nagios
usermod -aG nagios,nagcmd www-data

echo "Nagios manager : 2 Install dependencies"
NAGIOS_VERSION="4.4.14"

apt install -y build-essential apache2 php openssl perl php-gd libgd-dev \
libapache2-mod-php libperl-dev libssl-dev daemon apache2-utils

cd /tmp
wget https://assets.nagios.com/downloads/nagioscore/releases/nagios-$NAGIOS_VERSION.tar.gz -O nagios.tar.gz
tar -zxf nagios.tar.gz

echo "Nagios manager : 3 Build nagios"
cd /tmp/nagios-*
./configure --with-nagios-group=nagios --with-command-group=nagcmd --with-httpd_conf=/etc/apache2/sites-enabled/
make all

echo "Nagios manager : 4 Install built nagios"
# cd /tmp/nagios-*
make install
make install-init
make install-config
make install-commandmode
make install-webconf
/usr/bin/install -c -m 644 sample-config/httpd.conf /etc/apache2/sites-available/nagios.conf

echo "Nagios manager : 5 configure nagios"
echo password | htpasswd -ic /usr/local/nagios/etc/htpasswd.users nagiosadmin
sd "#(cfg_dir=/usr/local/nagios/etc/servers)" "\$1" /usr/local/nagios/etc/nagios.cfg

echo "Nagios manager : 6 configure nagios monitored hosts"
mkdir -p /usr/local/nagios/etc/servers
cp /vagrant/agent.cfg /usr/local/nagios/etc/servers/

# (Command to test nagios config)
# /usr/local/nagios/bin/nagios -v /usr/local/nagios/etc/nagios.cfg

echo "Nagios manager : 7 install nagios plugins"
apt install -y monitoring-plugins
ln -s /usr/lib/nagios/plugins/* /usr/local/nagios/libexec/

echo "Nagios manager : 8 enable apache modules"
a2enmod rewrite
a2enmod cgi

echo "Nagios manager : 9 start apache and nagios"
service apache2 restart
service nagios start
